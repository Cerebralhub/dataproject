<?php

namespace App\Http\Controllers;

//use App\tablesData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tableInfo = DB::table('tobi_asw')->get();
        return view('datatable', compact('tableInfo'));
    }


    // public function try(Request $request)
    // {
    //     //
    //     // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
    //     $tableInfo = DB::table('tobi_asw')->get();
    //     return view('datatable', compact('tableInfo'));
    // }



    public function akure_south(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('akure_south')->get();
        return view('akure_south', compact('tableInfo'));
    }






    public function akoko_south_west(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('akoko_south_west')->get();
        return view('akoko_south_west', compact('tableInfo'));
    }




    public function akure_north(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('akure_north')->get();
        return view('akure_north', compact('tableInfo'));
    }


  public function ese_odo(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('ese_odo')->get();
        return view('ese_odo', compact('tableInfo'));
    }



    public function idanre(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('idanre')->get();
        return view('idanre', compact('tableInfo'));
    }



    public function ifedore(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('ifedore')->get();
        return view('ifedore', compact('tableInfo'));
    }




    public function ilaje(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('ilaje')->get();
        return view('ilaje', compact('tableInfo'));
    }



    public function ileoluji_okeigbo(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('ileoluji_okeigbo')->get();
        return view('ileoluji_okeigbo', compact('tableInfo'));
    }




    public function irele(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('irele')->get();
        return view('irele', compact('tableInfo'));
    }



    public function odigbo(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('odigbo')->get();
        return view('odigbo', compact('tableInfo'));
    }



    public function okitipupa(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('okitipupa')->get();
        return view('okitipupa', compact('tableInfo'));
    }


    public function ondo_east(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('ondo_east')->get();
        return view('ondo_east', compact('tableInfo'));
    }
    



    public function ondo_west(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('ondo_west')->get();
        return view('ondo_west', compact('tableInfo'));
    }



    public function ose(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('ose')->get();
        return view('ose', compact('tableInfo'));
    }


    public function owo(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('owo')->get();
        return view('owo', compact('tableInfo'));
    }

    
    public function full_badagry(Request $request)
    {
        //
        // $tableInfo = DB::table('akure-south')->where('id', '<', '4002')->get();
        $tableInfo = DB::table('akure_south')->get();
        return view('full_badagry', compact('tableInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tablesData  $cr
     * @return \Illuminate\Http\Response
     */
    public function show(tablesData $cr)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tablesData  $cr
     * @return \Illuminate\Http\Response
     */
    public function edit(tablesData $cr)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tablesData  $cr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tablesData $cr)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tablesData  $cr
     * @return \Illuminate\Http\Response
     */
    public function destroy(tablesData $cr)
    {
        //
    }
}
