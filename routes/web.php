
<?php

use App\Http\Controllers\TablesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/data/badagry', 'TablesController@index')->name('badagry');
Route::get('/data/full-badagry', 'TablesController@full_badagry')->name('full-badagry');

Route::get('/data/akure_south', 'TablesController@akure_south')->name('akure_south');
Route::get('/data/akoko_south_west', 'TablesController@akoko_south_west')->name('akoko_south_west');
Route::get('/data/akure_north', 'TablesController@akure_north')->name('akure_north');
Route::get('/data/ese_odo', 'TablesController@ese_odo')->name('ese_odo');
Route::get('/data/idanre', 'TablesController@idanre')->name('idanre');
Route::get('/data/ifedore', 'TablesController@ifedore')->name('ifedore');
Route::get('/data/ilaje', 'TablesController@ilaje')->name('ilaje');
Route::get('/data/ileoluji_okeigbo', 'TablesController@ileoluji_okeigbo')->name('ileoluji_okeigbo');
Route::get('/data/irele', 'TablesController@irele')->name('irele');
Route::get('/data/odigbo', 'TablesController@odigbo')->name('odigbo');
Route::get('/data/okitipupa', 'TablesController@okitipupa')->name('okitipupa');
Route::get('/data/ondo_east', 'TablesController@ondo_east')->name('ondo_east');
Route::get('/data/ondo_west', 'TablesController@ondo_west')->name('ondo_west');
Route::get('/data/ose', 'TablesController@ose')->name('ose');
Route::get('/data/owo', 'TablesController@owo')->name('owo');

